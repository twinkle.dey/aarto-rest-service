package demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringzBootDemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringzBootDemoApplication.class, args);
    }
}
